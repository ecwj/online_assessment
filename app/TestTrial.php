<?php
namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TestAnswer
 *
 * @package App
 * @property string $question
 * @property string $option
 * @property tinyInteger $correct
*/
class TestTrial extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'created_at'];

    public static function boot()
    {
        parent::boot();

        TestTrial::observe(new \App\Observers\UserActionsObserver);
    }

}
