<?php

namespace App\Http\Controllers;

use Auth;
use App\Test;
use App\TestAnswer;
use App\Topic;
use Illuminate\Http\Request;
use App\Http\Requests\StoreResultsRequest;
use App\Http\Requests\UpdateResultsRequest;
use DB;

class ResultsController extends Controller
{
    public function __construct()
    {
        // $this->middleware('admin')->except('index', 'show');
        $this->middleware('auth_user');
    }

    /**
     * Display a listing of Result.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sql = "SELECT tests.id, CONCAT(users.name, ' (', users.email, ')') AS name, users.nric, result, completed_at, tests.created_at, topics.title
        FROM tests
        JOIN test_answers ON test_answers.test_id = tests.id
        JOIN questions ON questions.id = test_answers.question_id
        JOIN topics ON topics.id = questions.topic_id
        JOIN users ON users.id = tests.user_id
        WHERE users.deleted_at IS NULL AND tests.deleted_at IS NULL
        GROUP BY tests.id";
        $results = DB::select($sql, [Auth::id()]);

        $dataColumns = [
            'name' => trans('quickadmin.results.fields.user'),
            'nric' => trans('quickadmin.results.fields.nric'),
            'title' => trans('quickadmin.results.fields.title'),
            'result' => trans('quickadmin.results.fields.result'),
            'created_at' => trans('quickadmin.results.fields.created_at'),
            'completed_at' => trans('quickadmin.results.fields.completed_at'),
            '' => ''
        ];

        return view('results.index', compact('results', 'dataColumns'));
    }

    /**
     * Display Result.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        if ($id != 0) {
            if (Test::find($id)) {
                $test = Test::find($id)->load('user');
            } else {
                return redirect()->route('auth.registerCheck');
            }
        }

        $testCount = 0;

        if ($test) {
            $results = TestAnswer::where('test_id', $id)
                ->with('question')
                ->with('question.options')
                ->get()
            ;

            $topic = Topic::find($results[0]->question->topic_id);
            $testCount = $test->count();
        }

        return view('results.show', compact('test', 'results', 'topic', 'testCount'));
    }

    /**
     * Display All Result.
     *
     * @return \Illuminate\Http\Response
     */
    public function showAll()
    {   
        $sql = "SELECT result, completed_at, tests.created_at, topics.number_of_question, topics.title
                FROM test_trials
                JOIN tests ON tests.test_trial_id = test_trials.id
                JOIN test_answers ON test_answers.test_id = tests.id
                JOIN questions ON questions.id = test_answers.question_id
                JOIN topics ON topics.id = questions.topic_id
                WHERE test_trials.user_id = ? AND is_open = 0 AND test_trials.deleted_at IS NULL AND test_trials.is_pass = 1
                GROUP BY tests.id
                ORDER BY test_trials.id DESC";
        $selects = DB::select($sql, [Auth::id()]);

        $dataColumns = [
            'title' => trans('quickadmin.results.fields.title'),
            'result' => trans('quickadmin.results.fields.result'),
            'created_at' => trans('quickadmin.results.fields.created_at'),
            'completed_at' => trans('quickadmin.results.fields.completed_at'),
        ];

        return view('results.show_all', compact('selects', 'dataColumns'));
    }
}
