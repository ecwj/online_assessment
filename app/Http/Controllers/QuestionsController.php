<?php

namespace App\Http\Controllers;

use App\Question;
use App\QuestionsOption;
use Illuminate\Http\Request;
use App\Http\Requests\StoreQuestionsRequest;
use App\Http\Requests\UpdateQuestionsRequest;
use DB;

class QuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of Question.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::all();

        return view('questions.index', compact('questions'));
    }

    /**
     * Show the form for creating new Question.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $relations = [
            'topics' => \App\Topic::select(DB::raw("CONCAT(title,' ',title_zh) AS title"), 'id')->pluck('title', 'id')->prepend('Please select', ''),
        ];

        $correct_options = [
            'option1' => 'Option #1',
            'option2' => 'Option #2',
            'option3' => 'Option #3',
            'option4' => 'Option #4',
            'option5' => 'Option #5'
        ];

        return view('questions.create', compact('correct_options') + $relations);
    }

    /**
     * Store a newly created Question in storage.
     *
     * @param  \App\Http\Requests\StoreQuestionsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreQuestionsRequest $request)
    {

        $question = Question::create($request->all());

        DB::beginTransaction();
        $post = $request->input();
        foreach ($post as $key => $value) {
            if(strpos($key, 'option_zh') === false && strpos($key, 'option') !== false && $value != '') {
                $status = $request->input('correct') == $key ? 1 : 0;
                QuestionsOption::create([
                    'question_id' => $question->id,
                    'option'      => $value,
                    'option_zh'   => $post[str_replace("option", "option_zh", "$key")],
                    'correct'     => $status
                ]);                
            }
        }
        DB::commit();

        return redirect()->route('questions.index');
    }


    /**
     * Show the form for editing Question.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $relations = [
            'topics' => \App\Topic::select(DB::raw("CONCAT(title,' ',title_zh) AS title"), 'id')->pluck('title', 'id')->prepend('Please select', ''),
        ];

        $correct_options = [
            'option1' => 'Option #1',
            'option2' => 'Option #2',
            'option3' => 'Option #3',
            'option4' => 'Option #4',
            'option5' => 'Option #5'
        ];

        $question = Question::findOrFail($id);
        $questionOptions = QuestionsOption::where([
            ['question_id', '=', $id],
            ['deleted_at', '=', null]
        ])->get()->all();

        $correct = '';
        $index = 1;
        foreach ($questionOptions as $questionOption) {
            if ($questionOption->correct == 1) {
                $correct = "option$index";
            }
            $index ++;
        }

        return view('questions.edit', compact('question', 'questionOptions', 'correct_options', 'correct') + $relations);
    }

    /**
     * Update Question in storage.
     *
     * @param  \App\Http\Requests\UpdateQuestionsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateQuestionsRequest $request, $id)
    {
        DB::beginTransaction();
        $post = $request->all();

        $question = Question::findOrFail($id);
        $updateData = [];
        foreach ($post as $key => $value) {
            if (in_array($key, ['topic_id', 'question_text', 'question_text_zh'])) {
                $updateData[$key] = $value;
            }
        }
        $question->update($updateData);

        //update question
        $curdate = date('Y-m-d H:i:s');
        // $sql = "UPDATE questions_options SET deleted_at = ? WHERE question_id = ? AND deleted_at IS NULL";
        $sql = "DELETE FROM questions_options WHERE question_id = ? AND deleted_at IS NULL";
        DB::delete($sql, [$id]);
        // DB::update($sql, [$curdate, $id]);

        foreach ($post as $key => $value) {
            if(strpos($key, 'option_zh') === false && strpos($key, 'option') !== false && $value != '') {
                $status = $request->input('correct') == $key ? 1 : 0;
                QuestionsOption::create([
                    'question_id' => $question->id,
                    'option'      => $value,
                    'option_zh'   => $post[str_replace("option", "option_zh", "$key")],
                    'correct'     => $status
                ]);
            }
        }

        DB::commit();

        return back()->with('success', 'Successfully updated question.');
        return redirect()->route('questions.index');
    }


    /**
     * Display Question.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $relations = [
            'topics' => \App\Topic::select(DB::raw("CONCAT(title, ' ' , title_zh) AS title", 'id'))->pluck('title', 'id')->prepend('Please select', ''),
        ];

        $question = Question::findOrFail($id);
        $questionOptions = QuestionsOption::where('question_id', $id)->get()->all();

        return view('questions.show', compact('question', 'questionOptions') + $relations);
    }


    /**
     * Remove Question from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::findOrFail($id);
        $question->delete();

        return redirect()->route('questions.index');
    }

    /**
     * Delete all selected Question at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if ($request->input('ids')) {
            $entries = Question::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
