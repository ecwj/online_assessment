<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Test;
use App\TestAnswer;
use App\TestTrial;
use App\Topic;
use App\Question;
use App\QuestionsOption;
use Illuminate\Http\Request;
use App\Http\Requests\StoreTestRequest;
use App;

class TestsController extends Controller
{
    /**
     * Display a new test.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $topicId = $request->route('topic_id');
        $userId = Auth::id();
        $dateNow = date('Y-m-d H:i:s');

        DB::beginTransaction();
        if (!$topicId) {
            $topics = Topic::where([
                ['deleted_at', '=', null]
            ])->get()->all();

            $topic = $topics[0];
            $topicId = $topic->id;
        } else {
            $topics = Topic::find($topicId);
            $topic = $topics;
            if (!$topics) {
                echo 'invalid topic';
                exit;
            }
        }        

        $testTrial = TestTrial::where([
            ['user_id', '=', $userId],
            ['deleted_at', '=', null]
        ])->get()->all();

        $completedTest = 0;
        $testTrialId = null;
        foreach ($testTrial as $tt) {
            if ($tt->is_open == 0) {
                $completedTest ++;
            } else {
                $testTrialId = $tt->id;
            }
        }

        if ($completedTest >= 2) {
            Auth::guard()->logout();
            $request->session()->invalidate();
            return redirect()->route('auth.registerCheck')->withErrors('You have taken the online assessment twice, you are not allowed to take the assessment anymore.');
        }

        if (!is_null($testTrialId)) {
            //check if user has answer the stage
            $sql = "SELECT test_answers.id
                FROM questions
                JOIN test_answers ON questions.id = test_answers.question_id AND test_answers.user_id = ?
                JOIN tests ON tests.id = test_answers.test_id AND tests.test_trial_id = ?
                WHERE questions.topic_id = ? AND option_id IS NOT NULL";
            $selects = DB::select($sql, [$userId, $testTrialId, $topicId]);

            if ($selects) {
                $topicId = $this->getTopicId($userId, $topicId);
                if (!$topicId) {
                    return redirect()->route('results.show', ['0']);
                } else {
                    return redirect()->route('test', ['topic_id' => $topicId]);
                }
            }
        }

        $testAnswerNull = TestAnswer::where([
            ['user_id', '=', $userId],
            ['deleted_at', '=', null],
            ['option_id', '=', null]
        ])->get()->first();

        if ($testAnswerNull) {
            $topicId = Question::find($testAnswerNull->question_id)->topic_id;
            $topic = Topic::find($topicId);

            $testAnswer = TestAnswer::where([
                ['user_id', '=', $userId],
                ['deleted_at', '=', null],
                ['option_id', '=', null],
                ['test_id', '=', $testAnswerNull->test_id]
            ])->select(DB::raw('GROUP_CONCAT(question_id) AS question_ids'))->get()->all();

            $questions = Question::whereIn('id', explode(',', $testAnswer[0]->question_ids))->get()->all();

            foreach ($questions as &$question) {
                $question->options = QuestionsOption::where('question_id', $question->id)->inRandomOrder('id')->get()->all();
            }
            $questionLimit = $topic->number_of_question;
        } else {        

            $questionLimit = $topic->number_of_question;

            $questions = Question::inRandomOrder('id')->where([
                ['topic_id', '=', $topicId],
                ['deleted_at', '=', null]
            ])->limit($questionLimit)->get()->all();            

            $index = 0;
            foreach ($questions as $question) {
                $questions[$index]->options = QuestionsOption::where('question_id', $question->id)->inRandomOrder('id')->get()->all();
                $index ++;
            }

            if (is_null($testTrialId)) {
                $testTrial = TestTrial::create([
                    'user_id' => $userId,
                    'created_at'  => $dateNow
                ]);
                $testTrialId = $testTrial->id;
            }

            if (!$questions) {
                $test = Test::where([
                    ['test_trial_id', '=', $testTrialId],
                    ['completed_at', '!=', null]
                ])->orderBy('id', 'desc')->get()->first();
                return redirect()->route('results.show', [$test->id]);
            }

            $test = Test::create([
                'user_id' => $userId,
                'result'  => 0,
                'created_at'  => $dateNow,
                'test_trial_id' => $testTrialId
            ]);

            foreach ($questions as $question) {
                $status = 0;
                TestAnswer::create([
                    'user_id'     => Auth::id(),
                    'test_id'     => $test->id,
                    'question_id' => $question->id,
                    'option_id'   => null,
                    'correct'     => $status,
                    'created_at'  => $dateNow
                ]);
            }
        }

        DB::commit();

        return view('tests.create', compact('questions', 'topicId', 'questionLimit', 'topic'));
    }

    /**
     * Store a newly solved Test in storage with results.
     *
     * @param  \App\Http\Requests\StoreResultsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $result = 0;
        $userId = Auth::id();
        $dateNow = date('Y-m-d H:i:s');

        DB::beginTransaction();

        $testTrial = TestTrial::where([
            ['user_id', '=', $userId],
            ['deleted_at', '=', null],
            ['is_open', '=', 1]
        ])->orderBy('id', 'desc')->get()->first();

        $test = Test::where([
            ['test_trial_id', '=', $testTrial->id]
        ])->orderBy('id', 'desc')->get()->first();

        $testAnswers = TestAnswer::where([
            ['test_id', '=', $test->id],
        ])->get()->all();

        $incorrect = 0;

        foreach ($testAnswers as $testAnswer) {
            $questionId = $testAnswer->question_id;
            $answer = $request->input('answers.' . $questionId);
            $correctAnswer = QuestionsOption::find($answer)->correct;

            if ($correctAnswer) {
                $status = 1;
                $result ++;
            } else {
                $status = 0;
                $incorrect ++;
            }

            $sql = "UPDATE test_answers SET option_id = ?, correct = ?, updated_at = ? WHERE id = ?";
            DB::update($sql, [$answer, $status, $dateNow, $testAnswer->id]);
        }
        $topicId = Question::find($questionId)->topic_id;

        $sql = "UPDATE tests SET completed_at = ?, updated_at = ?, result = ? WHERE id = ?";
        DB::update($sql, [$dateNow, $dateNow, $result, $test->id]);

        DB::commit();
        if ($incorrect > 0) {
            $sql = "UPDATE test_trials SET is_open = ?, closed_at = ?, updated_at = ? WHERE id = ?";
            DB::update($sql, [0, $dateNow, $dateNow, $testTrial->id]);
            return redirect()->route('results.show', [$test->id]);
        } else {
            $topics = Topic::where([
                ['deleted_at', '=', null],
                ['id', '>', $topicId]
            ])->orderBy('id', 'asc')->get()->first();
            if ($topics) {

                //check if any question for next stage
                $questions = Question::where([
                    ['topic_id', '=', $topics->id],
                    ['deleted_at', '=', null]
                ])->get()->all();

                if (!$questions) {
                    $sql = "UPDATE test_trials SET is_open = ?, is_pass = ?, closed_at = ?, updated_at = ? WHERE id = ?";
                    DB::update($sql, [0, 1, $dateNow, $dateNow, $testTrial->id]);
                    return redirect()->route('results.showAll');
                }
                return redirect()->route('test', ['topic_id' => $topics]);
            } else {
                echo 'All passed.';
                exit;
            }
        }
    }

    private function getTopicId($userId, $topicId)
    {
        $sql = "SELECT topics.id AS topic_id, 
(
    SELECT test_answers.id 
    FROM questions 
    JOIN test_answers ON questions.id = test_answers.question_id AND test_answers.user_id = ?
    WHERE questions.topic_id = topics.id AND option_id IS NOT NULL LIMIT 1) AS is_answer
FROM topics 
JOIN questions ON questions.topic_id = topics.id AND questions.deleted_at IS NULL
LEFT JOIN test_answers ON questions.id = test_answers.question_id AND test_answers.user_id = ?
WHERE topics.id > ? AND topics.deleted_at IS NULL
GROUP BY topics.id";

        $selects = DB::select($sql, [$userId, $userId, $topicId]);
        if ($selects) {
            return $selects[0]->topic_id;
        }
        return false;
    }
}
