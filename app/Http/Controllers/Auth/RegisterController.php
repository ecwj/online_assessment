<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Role;
use App\Test;
use App\TestAnswer;
use App\TestTrial;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            // 'name'     => 'required|max:255',
            // 'email'    => 'required|email|max:255|unique:users',
            // 'password' => 'required|min:6|confirmed',
            'name'     => 'required|max:30',
            'email'    => 'required|email|max:100|unique:users',
            'nric'    => 'required|max:20|unique:users',
            'address' => 'required|max:30',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create($data);
        return User::create([
            'name'     => $data['name'],
            'email'    => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function registerCheck(Request $request)
    {
        // echo \App::getLocale();exit;
        DB::beginTransaction();
        if ($request->isMethod('post')) {
            $nric = $request->input('nric');
            $submit = $request->input('submit');
            
            if ($submit == 'check') {
                $user = User::where([
                    ['nric', '=', $nric],
                    ['deleted_at', '=', null]

                ])->get()->first();
                if (!$user) {
                    Session(['newUser' => true]);
                    return view('auth/register_check', ['nric' => $nric]);
                } else {

                    $testTrials = TestTrial::where([
                        ['user_id', '=', $user->id],
                        ['deleted_at', '=', null]
                    ])->get()->all(); 

                    foreach ($testTrials as $testTrial) {
                        if ($testTrial->is_pass == 1) {
                            Auth::loginUsingId($user->id);
                            return redirect(route('results.showAll'));
                        }
                    }
                    if ($testTrials && sizeof($testTrials) >= 2) {
                        return back()->withErrors(['error' => 'You have taken the online assessment twice, you are not allowed to take the assessment anymore.']);
                    }

                    Auth::loginUsingId($user->id);
                    return redirect(route('test'));
                }
                Session(['newUser' => false]);
            } else if ($submit == 'register') {
                $dateNow = date('Y-m-d H:i:s');
                $post = $request->all();
                $this->validator($post)->validate();

                $data = [];
                $post['role_id'] = Role::ROLE_USER;
                foreach ($post as $key => $value) {
                    if (in_array($key, ['nric', 'name', 'email', 'address', 'role_id', 'created_at'])) {
                        $data[$key] = $value;
                    }
                }
                $user = $this->create($data);

                Auth::loginUsingId($user->id);

                DB::commit();
                return redirect(route('test'));
            }
        }
        
        return view('auth/register_check');
    }

    public function registerUser(Request $request)
    {
        $this->validator($request->all())->validate();

        if ($request->isMethod('post')) {
            $nric = $request->input('nric');
            $email = $request->input('email');
            $user = User::where('nric', $nric)->get()->first();
            if (!$user) {
                return view('auth/register_check', ['newUser' => true, 'nric' => $nric]);
            }
        }
        return view('auth/register_check');
    }
}
