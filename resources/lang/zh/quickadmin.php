<?php

return [
	'create' => '创建',
	'save' => '保存',
	'edit' => '编辑',
	'view' => '查看',
	'update' => '更新',
	'list' => '列表',
	'no_entries_in_table' => '表中没有条目',
	'custom_controller_index' => '自定义控制器索引。',
	'logout' => '登出',
	'add_new' => '添新',
	'are_you_sure' => '您确定？',
	'back_to_list' => '回到列表',
	'dashboard' => '仪表板',
	'delete' => '删除',
	'quickadmin_title' => '在线评估',
	'please_select' => '请选择',

	'user-management' => [
		'title' => '用户管理',
		'fields' => [
		],
	],

    'test' => [
        'new' => 'New Quiz',
    ],

	'roles' => [
		'title' => '身份',
		'fields' => [
			'title' => '标题',
		],
	],

	'users' => [
		'title' => '用户',
		'fields' => [
			'name' => '名字',
			'email' => '电子邮箱',
			'nric' => '身份证/护照',
			'address' => '地址',
			'password' => '密码',
			'role' => '身份',
			'remember-token' => 'Remember token',
			'created_at' => '加入日期'
		],
	],

	'user-actions' => [
		'title' => '用户动作',
		'fields' => [
			'user' => '用户',
			'action' => '动作',
			'action-model' => '动作模版',
			'action-id' => '动作 ID',
		],
	],

	'topics' => [
		'title' => '阶段',
		'fields' => [
			'title' => '标题（英文）',
			'title_zh' => '标题',
			'number_of_question' => '问题数量',
		],
	],

	'questions' => [
		'title' => '问题',
		'fields' => [
			'topic' => '阶段（英文）',
			'topic_zh' => '阶段',
			'question-text' => '问题内容（英文）',
			'question-text_zh' => '问题内容',
			'code-snippet' => 'Code snippet',
			'answer-explanation' => 'Answer explanation',
			'more-info-link' => 'More info link',
		],
	],

	'questions-options' => [
		'title' => '问题选项',
		'fields' => [
			'question' => '问题',
			'option' => '选项（英文）',
			'option_zh' => '选项',
			'correct' => '正解',
		],
	],

	'results' => [
		'title' => '我的成绩',
		'title_2' => '成绩 :title',
		'titles' => '成绩',
		'fields' => [
			'user' => '用户',
			'question' => '问题',
			'correct' => '正解',
			'date' => '日期',
			'result' => '分数',
			'nric' => '身份证/护照',
			'title' => '阶段',
			'created_at' => '开始时间',
			'completed_at' => '结束时间'
		],
	],

	'register' => [
		'title_check' => '注册检验'
	],

	'laravel-quiz' => 'New Laravel Quiz',
	'quiz' => '回答这:no道问题。没有时间限制。',
	'submit_quiz' => '提交答案',
	'view-result' => '查看成绩',

];
