@extends('layouts.auth')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('quickadmin.register.title_check')</div>
                <div class="panel-body">

                    @php $newUser = session('newUser'); @endphp
                    @if (isset($nric) && $nric != '')
                        @php $nricDisabled = 'readonly'; @endphp
                    @else 
                        @php $nricDisabled = ''; @endphp
                    @endif
                    
                    @if (isset($newUser) == false || $newUser == false || !isset($nric) || $nric == '')
                    <div class="form-group">
                        <a class="btn btn-primary" href="{{ route('setLocale', 'en') }}">English</a>
                        <a class="btn btn-danger" href="{{ route('setLocale', 'zh') }}">中文</a>
                    </div>
                    @endif

                    <form class="form-horizontal" id="form-register" role="form" method="POST" action="{{ route('auth.registerCheck') }}">
                        {{ csrf_field() }}                        
                        @if($errors->has('error'))
                            <div class="alert alert-danger">
                                {{ $errors->first('error') }}
                            </div>
                        @endif

                        <div class="form-group{{ $errors->has('nric') ? ' has-error' : '' }}">
                            <label for="nric" class="col-md-4 control-label">@lang('quickadmin.users.fields.nric')</label>

                            <div class="col-md-6">
                                <input id="nric" type="text" class="form-control" name="nric" value="{{ old('nric') ? old('nric') : ((isset($nric) && $nric) ? $nric : '') }}" required autofocus {{ $nricDisabled }}>

                                @if ($errors->has('nric'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nric') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        @if (isset($newUser) == false || $newUser == false || !isset($nric) || $nric == '')
                        @if (config('app.env') != 'local')
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="g-recaptcha" data-sitekey="{{ config('app.google_recaptcha_site_key') }}"></div> 
                                </div>
                            </div>
                        @endif
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" name="submit" class="btn btn-primary" value="check">
                                    Check
                                </button>
                            </div>
                        </div>
                        @endif

                        @if (isset($newUser) && $newUser == true && (isset($nric) && $nric != ''))
                        <div class="alert alert-warning">
                            <p>Please fill up the following information in order to proceed</p>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">@lang('quickadmin.users.fields.email')</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">@lang('quickadmin.users.fields.name')</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-4 control-label">@lang('quickadmin.users.fields.address')</label>

                            <div class="col-md-6">
                                <textarea id="address" class="form-control" name="address" rows="3" required>{{ old('address') }}</textarea>

                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" name="submit" class="btn btn-primary" value="register">
                                    Register
                                </button>
                            </div>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
    $(function() {
        $('input[name="nric"]').keyup(function() {
            var val = $(this).val();
            val = val.replace("-", "");
            val = val.replace(".", "");
            val = val.replace(" ", "");
            $(this).val(val);
        });
        @if (config('app.env') != 'local')
            $("#form-register").submit(function(evt) {
                var response = grecaptcha.getResponse();
                if(response.length == 0) 
                    { 
                        //reCaptcha not verified
                        alert("Please verify reCaptcha!"); 
                        evt.preventDefault();
                        return false;
                    }
                });
            });
        @endif
</script>
@endsection
