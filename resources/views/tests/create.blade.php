@extends('layouts.app')

@section('content')
@php $locale = App::getLocale(); @endphp
    <h3 class="page-title">{{ $locale == 'zh' ? $topic->title_zh : $topic->title }}</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['tests.store']]) !!}
    
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.quiz', ['no' => $questionLimit])
        </div>
        <?php //dd($questions) ?>
    @if(count($questions) > 0)
        <div class="panel-body">
        <?php $i = 1; ?>
        @foreach($questions as $question)
            @if ($i > 1) <hr /> @endif
            <div class="row">
                <div class="col-xs-12 form-group">
                    <div class="form-group">
                        <strong>@lang('quickadmin.questions.title') {{ $i }}.<br />{!! nl2br($locale == 'zh' ? $question->question_text_zh : $question->question_text) !!}</strong>

                        <input
                            type="hidden"
                            name="questions[{{ $question->id }}]"
                            value="{{ $question->id }}">
                    @foreach($question->options as $option)
                        <br>
                        <label class="radio-inline">
                            <input
                                type="radio"
                                name="answers[{{ $question->id }}]"
                                value="{{ $option->id }}" required>
                            {{ $locale == 'zh' ? $option->option_zh : $option->option }}
                        </label>
                    @endforeach
                    </div>
                </div>
            </div>
        <?php $i++; ?>
        @endforeach
        </div>
    @endif
    </div>

    {!! Form::submit(trans('quickadmin.submit_quiz'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent
    <script src="{{ url('quickadmin/js') }}/timepicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.4.5/jquery-ui-timepicker-addon.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
    <script>
        $('.datetime').datetimepicker({
            autoclose: true,
            dateFormat: "{{ config('app.date_format_js') }}",
            timeFormat: "hh:mm:ss"
        });
    </script>

@stop
