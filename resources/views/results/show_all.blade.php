@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.results.titles')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.view-result')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered table-striped">
                        <thead>
                            @if(Auth::user()->isAdmin())
                            <tr>
                                <th>@lang('quickadmin.results.fields.user')</th>
                                <td colspan="3">{{ $test->user->name or '' }} ({{ $test->user->email or '' }})</td>
                            </tr>
                            @endif
                            <tr>
                                @foreach ($dataColumns as $key => $col)
                                    <th>{{ $col }}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($selects as $sel)
                            <tr>
                                <td>{{ $sel->title }}</td>
                                <td>{{ date('Y-m-d h:i a', strtotime($sel->created_at)) }}</td>
                                <td>{{ date('Y-m-d h:i a', strtotime($sel->completed_at)) }}</td>
                                <td>{{ $sel->result . '/' . $sel->number_of_question }}</td>
                            </tr>
                            @endforeach
                            @if(!Auth::user()->isAdmin())
                            <tr>
                                <td colspan="4" class="alert-success">
                                    You have passed all stages.
                                </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                @if(Auth::user()->isAdmin())
                <?php $i = 1 ?>
                @foreach($results as $result)
                    <table class="table table-bordered table-striped">
                        <tr class="test-option{{ $result->correct ? '-true' : '-false' }}">
                            <th style="width: 10%">Question #{{ $i }}</th>
                            <th>{{ $result->question->question_text or '' }}</th>
                        </tr>
                        <tr>
                            <td>Options</td>
                            <td>
                                <ul>
                                @foreach($result->question->options as $option)
                                    <li style="@if ($option->correct == 1) font-weight: bold; @endif
                                        @if ($result->option_id == $option->id) text-decoration: underline @endif">{{ $option->option }}
                                        @if ($option->correct == 1) <em>(correct answer)</em> @endif
                                        @if ($result->option_id == $option->id) <em>(your answer)</em> @endif
                                    </li>
                                @endforeach
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>Answer Explanation</td>
                            <td>
                            {!! $result->question->answer_explanation  !!}
                                @if ($result->question->more_info_link != '')
                                    <br>
                                    <br>
                                    Read more:
                                    <a href="{{ $result->question->more_info_link }}" target="_blank">{{ $result->question->more_info_link }}</a>
                                @endif
                            </td>
                        </tr>
                    </table>
                <?php $i++ ?>
                @endforeach
                @endif
                </div>
            </div>

            <p>&nbsp;</p>

            @if(Auth::user()->isAdmin())
            <a href="{{ route('results.index') }}" class="btn btn-default">See all my results</a>
            @endif
        </div>
    </div>
@stop
