@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.results.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>

        <div class="panel-body">
            <table class="table table-bordered table-striped {{ count($results) > 0 ? 'datatable' : '' }}">
                <thead>
                    <tr>
                        @foreach ($dataColumns as $key => $col)
                        <th>{{ $col }}</th>
                        @endforeach
                    </tr>
                </thead>

                <tbody>
                    @if ($results)
                        @foreach ($results as $result)
                            <tr>
                                @foreach ($dataColumns as $key => $col)
                                    @if ($key != '')
                                    <td>{{ $result->$key }}</td>
                                    @endif
                                @endforeach
                                <td>
                                    <a href="{{ route('results.show',[$result->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.view')</a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">@lang('quickadmin.no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop
