@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.topics.title')</h3>
    
    {!! Form::model($topic, ['method' => 'PUT', 'route' => ['topics.update', $topic->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('title', trans('quickadmin.topics.fields.title') . '*', ['class' => 'control-label']) !!}
                    {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('title'))
                        <p class="help-block">
                            {{ $errors->first('title') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-12 form-group">
                    {!! Form::label('title_zh', trans('quickadmin.topics.fields.title_zh') . '*', ['class' => 'control-label']) !!}
                    {!! Form::text('title_zh', old('title_zh'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('title_zh'))
                        <p class="help-block">
                            {{ $errors->first('title_zh') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-12 form-group">
                    {!! Form::label('number_of_question', trans('quickadmin.topics.fields.number_of_question') . '*', ['class' => 'control-label']) !!}
                    <select name="number_of_question" class="form-control">
                        <option value="">@lang('quickadmin.please_select')</option>
                        @for ($i = 1; $i <= 15; $i ++)
                            <option value="{{ $i }}" @if ($topic->number_of_question == $i) selected @endif>{{ $i }}</option>
                        @endfor
                    </select>
                    <p class="help-block"></p>
                    @if($errors->has('number_of_question'))
                        <p class="help-block">
                            {{ $errors->first('number_of_question') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

