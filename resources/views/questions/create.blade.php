@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.questions.title')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['questions.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.create')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('topic_id', 'Topic*', ['class' => 'control-label']) !!}
                    {!! Form::select('topic_id', $topics, old('topic_id'), ['class' => 'form-control']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('topic_id'))
                        <p class="help-block">
                            {{ $errors->first('topic_id') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::label('question_text', 'Question text*', ['class' => 'control-label']) !!}
                            {!! Form::textarea('question_text', old('question_text'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('question_text'))
                                <p class="help-block">
                                    {{ $errors->first('question_text') }}
                                </p>
                            @endif
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('question_text_zh', 'Question text (Chinese)*', ['class' => 'control-label']) !!}
                            {!! Form::textarea('question_text_zh', old('question_text_zh'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('question_text_zh'))
                                <p class="help-block">
                                    {{ $errors->first('question_text_zh') }}
                                </p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::label('option1', 'Option #1', ['class' => 'control-label']) !!}
                            {!! Form::text('option1', old('option1'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('option1'))
                                <p class="help-block">
                                    {{ $errors->first('option1') }}
                                </p>
                            @endif
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('option_zh1', 'Option (Chinese) #1', ['class' => 'control-label']) !!}
                            {!! Form::text('option_zh1', old('option_zh1'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('option_zh1'))
                                <p class="help-block">
                                    {{ $errors->first('option_zh1') }}
                                </p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::label('option2', 'Option #2', ['class' => 'control-label']) !!}
                            {!! Form::text('option2', old('option2'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('option2'))
                                <p class="help-block">
                                    {{ $errors->first('option2') }}
                                </p>
                            @endif
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('option_zh2', 'Option (Chinese) #2', ['class' => 'control-label']) !!}
                            {!! Form::text('option_zh2', old('option_zh2'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('option_zh2'))
                                <p class="help-block">
                                    {{ $errors->first('option_zh2') }}
                                </p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::label('option3', 'Option #3', ['class' => 'control-label']) !!}
                            {!! Form::text('option3', old('option3'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('option3'))
                                <p class="help-block">
                                    {{ $errors->first('option3') }}
                                </p>
                            @endif
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('option_zh3', 'Option (Chinese) #3', ['class' => 'control-label']) !!}
                            {!! Form::text('option_zh3', old('option_zh3'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('option_zh3'))
                                <p class="help-block">
                                    {{ $errors->first('option_zh3') }}
                                </p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::label('option4', 'Option #4', ['class' => 'control-label']) !!}
                            {!! Form::text('option4', old('option4'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('option4'))
                                <p class="help-block">
                                    {{ $errors->first('option4') }}
                                </p>
                            @endif
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('option_zh4', 'Option (Chinese) #4', ['class' => 'control-label']) !!}
                            {!! Form::text('option_zh4', old('option_zh4'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('option_zh4'))
                                <p class="help-block">
                                    {{ $errors->first('option_zh4') }}
                                </p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::label('option5', 'Option #5', ['class' => 'control-label']) !!}
                            {!! Form::text('option5', old('option5'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('option5'))
                                <p class="help-block">
                                    {{ $errors->first('option5') }}
                                </p>
                            @endif
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('option_zh5', 'Option (Chinese) #5', ['class' => 'control-label']) !!}
                            {!! Form::text('option_zh5', old('option_zh5'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('option_zh5'))
                                <p class="help-block">
                                    {{ $errors->first('option_zh5') }}
                                </p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('correct', 'Correct', ['class' => 'control-label']) !!}
                    {!! Form::select('correct', $correct_options, old('correct'), ['class' => 'form-control']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('correct'))
                        <p class="help-block">
                            {{ $errors->first('correct') }}
                        </p>
                    @endif
                </div>
            </div>
            {{--<div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('code_snippet', 'Code snippet', ['class' => 'control-label']) !!}
                    {!! Form::textarea('code_snippet', old('code_snippet'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('code_snippet'))
                        <p class="help-block">
                            {{ $errors->first('code_snippet') }}
                        </p>
                    @endif
                </div>
            </div>--}}
            {{--<div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('answer_explanation', 'Answer explanation*', ['class' => 'control-label']) !!}
                    {!! Form::textarea('answer_explanation', old('answer_explanation'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('answer_explanation'))
                        <p class="help-block">
                            {{ $errors->first('answer_explanation') }}
                        </p>
                    @endif
                </div>
            </div>--}}
            {{--<div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('more_info_link', 'More info link', ['class' => 'control-label']) !!}
                    {!! Form::text('more_info_link', old('more_info_link'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('more_info_link'))
                        <p class="help-block">
                            {{ $errors->first('more_info_link') }}
                        </p>
                    @endif
                </div>
            </div>--}}

        </div>
    </div>

    {!! Form::submit(trans('quickadmin.save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

